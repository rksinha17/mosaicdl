# MosaicDL

* [Code of conduct](https://bitbucket.org/maxiqdev/mosaicdl/src/master/CODE_OF_CONDUCT.md)
* [Pull requests](https://bitbucket.org/maxiqdev/mosaicdl/src/master/PULL_REQUEST_TEMPLATE.md)
* [Issues](https://bitbucket.org/maxiqdev/mosaicdl/src/master/ISSUE_TEMPLATE.md)
* PR's with test coverage < 90% will not be merged
