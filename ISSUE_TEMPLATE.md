<!--
Have you read our Code of Conduct? By filing an Issue, you are expected to comply with it, including treating everyone with respect: https://bitbucket.org/maxiqdev/mosaicdl/src/master/CODE_OF_CONDUCT.md
-->

### Prerequisites

[Description of the prerequisites like any external lib or something]

### Description

[Description of the issue]

### Steps to Reproduce

1. [First Step]
2. [Second Step]
3. [and so on...]

**Expected behavior:** [What you expect to happen]

**Actual behavior:** [What actually happens]

**Reproduces how often:** [What percentage of the time does it reproduce?]

### Versions

In which versions are you facing the issue. Also, please include the OS and what version of the OS you're running.

### Additional Information

Any additional information, configuration or data that might be necessary to reproduce the issue.
