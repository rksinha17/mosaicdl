import {storage} from './storage';
import { endpoints } from './endpoints';

let getHeaders = () => {
  return {
    'Authorization': 'Token'.concat(' ', storage.read('token'))
  }
}

export const common = {};

/* logout */
common.auth = {};
common.auth.logout = () => {
  swal({
    title: 'Are you sure?',
    text: "You will be kicked out",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes',
    cancelButtonText: 'No',
    confirmButtonClass: 'btn btn-success',
    cancelButtonClass: 'btn btn-danger',
    buttonsStyling: false,
    reverseButtons: true
  })
  .then((value) => {
    $.ajax({
      url: endpoints.auth.logout,
      method: 'POST',
      headers: getHeaders()
    })
    .done(function(data, status, xhr) {
      storage.clear();
      window.location = '/';
    })
    .fail(function(xhr, status, error) {
      console.error(error);
    });
  });
}
