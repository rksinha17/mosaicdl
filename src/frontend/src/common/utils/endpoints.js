export const endpoints = {};

/* base url */
endpoints.base = '/api';

/* endpoints available in the auth module */
endpoints.auth = {};
endpoints.auth.login = endpoints.base.concat('/users/login');
endpoints.auth.logout = endpoints.base.concat('/users/logout');

/* endpoints available in the users module */
endpoints.users = {};
endpoints.users.list = endpoints.base.concat('/users/');
