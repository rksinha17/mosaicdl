# Frontend for MosaicDL

Frontend is build using [React](https://reactjs.org/) and [Redux](https://redux.js.org/)

## Getting Started

* Run `make install`. This will install system dependencies and npm dependencies
* Run `npm start`. This will start the development server. Your application will now be live in `http://127.0.0.1:8000`
* Run `npm run build` to transpile files for production use

## Bug reporting

* If you come across any bugs feel free to raise them under the `issues` tab in bitbucket
