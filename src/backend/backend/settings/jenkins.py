# -*- coding: utf-8 -*-
""" configuration for jenkins env """
from .base import *


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'mosaicdl',
        'USER': 'root',
        'PASSWORD': 'root',
        'TEST': {
            'NAME': 'mosaicdltest',
        },
    }
}
