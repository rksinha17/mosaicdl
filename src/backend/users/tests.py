# -*- coding: utf-8 -*-
""" This module holds all the test cases associated with authentication """
import json

from rest_framework.status import HTTP_200_OK
from rest_framework.test import APITestCase

from utils.mixins import TestMixin


class AuthenticationTestCase(TestMixin, APITestCase):
    """ test cases for user authentication """
    perform_login = False

    @staticmethod
    def get_expected_login_response(user):
        """ expected response for login api """
        user_dict = AuthenticationTestCase.get_user_dict(user)
        token_key = AuthenticationTestCase.get_token_key(user)
        return json.dumps({'user': user_dict, 'token': token_key})

    def test_user_can_login(self):
        """ test login functionality """

        # prepare data
        data = {'username': 'adam.john', 'password': 'adam.john.007'}

        # hit api
        response = self.client.post('/users/login', data, format='json')

        # expected output
        expected_response = self.get_expected_login_response(self.user)

        # verify response
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertJSONEqual(response.content, expected_response)

    def test_user_can_logout(self):
        """ test logout functionality """

        # prepare data
        auth_header = 'Token {}'.format(self.get_token_key(self.user, True))
        # pylint: disable=no-member
        self.client.credentials(HTTP_AUTHORIZATION=auth_header)

        # hit api
        response = self.client.post('/users/logout')

        # verify response
        self.assertEqual(response.status_code, HTTP_200_OK)
