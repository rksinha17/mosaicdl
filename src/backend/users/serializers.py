# -*- coding: utf-8 -*-
""" serializers associated with the user model """
from django.contrib.auth.models import User
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    """ serializer for the user model """
    full_name = serializers.SerializerMethodField()

    # pylint: disable=too-few-public-methods,
    class Meta:
        """ metadata associated with user serializer """
        model = User
        fields = ('id', 'is_superuser', 'is_active', 'username', 'first_name', 'last_name',
                  'email', 'is_active', 'full_name')

    # pylint: disable=too-many-ancestors,no-self-use
    def get_full_name(self, obj):
        """ Compute full name from first name and last name """
        return '{} {}'.format(obj.first_name, obj.last_name)
