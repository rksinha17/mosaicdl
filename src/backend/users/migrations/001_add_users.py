# -*- coding: utf-8 -*-
# pylint: disable=invalid-name
""" database migrations associated with users """
from django.contrib.auth.models import User
from django.db import migrations


# pylint: disable=unused-argument
def create_users(apps, schema_editor):
    """ create users """
    User.objects.create_superuser(
        'adam.john',
        'adam.john@lntinfotech.com',
        'adam.john.007',
        first_name='Adam',
        last_name='John'
    )


class Migration(migrations.Migration):
    """ migration script """
    operations = [
        migrations.RunPython(create_users),
    ]
