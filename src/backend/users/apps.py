# -*- coding: utf-8 -*-
""" django application registry """
from django.apps import AppConfig


class UsersConfig(AppConfig):
    """ django app cocnfiguration """
    name = 'users'
