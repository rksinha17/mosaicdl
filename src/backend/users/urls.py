# -*- coding: utf-8 -*-
from django.urls import path, re_path

from . import views


urlpatterns = [
    path('login', views.LoginAPIView.as_view()),
    path('logout', views.LogoutAPIView.as_view()),
    path('', views.UserListCreateAPIView.as_view()),
    re_path('^(?P<pk>[0-9]+)$', views.UserRetrieveUpdateDestroyAPIView.as_view())
]
