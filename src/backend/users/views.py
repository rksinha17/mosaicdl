# -*- coding: utf-8 -*-
""" apis associated with the user model """
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView,
)
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from . import serializers


class UserListCreateAPIView(ListCreateAPIView):
    """ Create new user or get the list of users """
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer


# pylint: disable=too-many-ancestors
class UserRetrieveUpdateDestroyAPIView(RetrieveUpdateDestroyAPIView):
    """ Perform Read, Update, Delete on top of users """
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer


class LoginAPIView(APIView):
    """ API for login """
    permission_classes = (AllowAny,)

    @staticmethod
    def get_validated_data(data):
        """ validate user input """
        serializer = AuthTokenSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        return serializer.data

    @staticmethod
    def get_user_object(username, password):
        """ verify credentials and validate user """
        # pylint: disable=too-many-function-args
        return authenticate(username=username, password=password)

    @staticmethod
    def delete_tokens(user):
        """ delete tokens associated with the given user """
        # pylint: disable=no-member
        Token.objects.filter(user=user).delete()

    @staticmethod
    def get_token_key(user):
        """ generate token for the given user """
        # pylint: disable=no-member
        token = Token.objects.create(user=user)
        return token.key

    @staticmethod
    def get_user_dict(user):
        """ convert user object to dictionary """
        return serializers.UserSerializer(user).data

    # pylint: disable=unused-argument
    def post(self, request, *args, **kwargs):
        """ login user to the system """
        data = self.get_validated_data(request.data)
        user = self.get_user_object(data['username'], data['password'])
        if user:
            self.delete_tokens(user)
            response = {
                'user': self.get_user_dict(user),
                'token': self.get_token_key(user),
            }
            return Response(response, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_404_NOT_FOUND)


class LogoutAPIView(APIView):
    """ API for logout"""

    @staticmethod
    def delete_tokens(user):
        """ delete tokens associated with the given user """
        # pylint: disable=no-member
        Token.objects.filter(user=user).delete()

    # pylint: disable=unused-argument
    def post(self, request, *args, **kwargs):
        """ logout user from the system """
        self.delete_tokens(request.user)
        return Response(status=status.HTTP_200_OK)
