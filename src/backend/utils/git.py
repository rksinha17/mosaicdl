import pygit2
import os

def create_repo(name,public_key, private_key,remote_dir_url):
    keypair = pygit2.Keypair("git", public_key, private_key, "")
    callbacks = pygit2.RemoteCallbacks(credentials=keypair)
    local_cloned_dir = "/tmp/{}".format(name)
    try:
        # print("Now cloning your repo");
        repo = pygit2.clone_repository(remote_dir_url, local_cloned_dir, callbacks=callbacks)
    except ValueError:
        print("Oops!  Repository already exists");
    return;
def checkout(local_cloned_dir):
    try:
        print("checkout master branch")
        repo = pygit2.Repository(local_cloned_dir)
        branch = repo.lookup_branch('master')
        ref = repo.lookup_reference(branch.name)
        repo.checkout(ref)
    except ValueError:
        print("Error checking out new branch")
        exit()
    return;


def create_file(local_cloned_dir):
    f = open("rohan7.txt", "w+")
    for i in range(10):
        f.write("This is new line %d\r\n" % (i + 1))
    f.close()
    return;
def push_file(local_cloned_dir) :
    try:
        repo = pygit2.Repository(local_cloned_dir)
        index = repo.index
        index.add("rohan7.txt")
        index.write()
        message = '...some commit message...'
        tree = index.write_tree()
        author = pygit2.Signature("rohan", "rohan@git")
        commiter = pygit2.Signature("rohan", "rohan@git")
        oid = repo.create_commit('refs/heads/master', author, commiter, message, tree, [repo.head.get_object().hex])
        os.system("git push origin master")
    except ValueError:
        print ("Error in pushing")
    return ;

def git_list(local_cloned_dir):
    try:
        os.system("git log --pretty=oneline > git_list.txt ")

    except ValueError:
        print('error in git list')
    return;


def pull_file(local_cloned_dir) :
    try:
        os.system("git pull")
    except ValueError:
        print ("Error in pulling")
    return;