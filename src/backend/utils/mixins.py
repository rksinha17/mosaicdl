# -*- coding: utf-8 -*-
""" several mixins used across the project """
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

from users.serializers import UserSerializer


class TestMixin(object):
    """ mixin provides common functions used in tests """
    perform_login = True

    # pylint: disable=invalid-name
    def setUp(self):
        """ setup sample data """
        self.user = User.objects.filter(username='adam.john').get()
        if self.perform_login is True:
            self.login()

    @staticmethod
    def get_user_dict(user):
        """ convert given user object to dictionary """
        return UserSerializer(user).data

    # pylint: disable=no-member
    @staticmethod
    def get_token_key(user, create=False):
        """ retrieve token associated with the given user """
        if create:
            token = Token(user=user)
            token.save()
            return token.key
        return Token.objects.filter(user=user).get().key

    def login(self):
        """ login to the system """
        auth_header = 'Token {}'.format(self.get_token_key(self.user, True))
        self.client.credentials(HTTP_AUTHORIZATION=auth_header)
