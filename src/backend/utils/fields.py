# -*- coding: utf-8 -*-
""" custom fields used in mosaicdl """
import json

from django.db import models


class JSONField(models.TextField):
    """ JSON field implementation on top of django textfield """

    @staticmethod
    def to_dict(value):
        """ convert json string to python dictionary """
        return json.loads(value)

    @staticmethod
    def to_json(value):
        """ convert python dictionary to json string """
        return json.dumps(value)

    # pylint: disable=unused-argument
    def from_db_value(self, value, expression, connection):
        """ convert string from db to python dictionary """
        if value is None:
            return value
        return self.to_dict(value)

    def to_python(self, value):
        """ convert model input value to python dictionary """
        if isinstance(value, dict):
            return value
        if value is None:
            return value
        return self.to_dict(value)

    def get_prep_value(self, value):
        """ convert python dictionary to string before writing to db """
        return self.to_json(value)
