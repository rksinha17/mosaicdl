# Backend for MosaicDL

Backend is build using [Django](https://www.djangoproject.com/) and [Django REST framework](http://www.django-rest-framework.org/)

## Getting Started

* Update database details in the file `backend.settings.local`
* Run `make install`. This will install system dependencies, python dependencies, database schema and sample data
* Run `make run`. This will start the development server. Your application will now be live in `http://localhost:8000`
* You can login using the default username `adam.john` and password `adam.john.007`

## Guidelines for raising PR

* Make sure you have written enough test cases
* Make sure all tests are passing, you can run tests via `make test`
* Make sure that the test coverage is greater than 90%

## Bug reporting

* If you come across any bugs feel free to raise them under the `issues` tab in bitbucket
