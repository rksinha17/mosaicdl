# -*- coding: utf-8 -*-
""" tests associated with the datasources """
import json

from freezegun import freeze_time
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT
from rest_framework.test import APITestCase

from utils.mixins import TestMixin

from . import models


@freeze_time('2018-01-01T00:00:00Z')
class DataSourceTestCase(TestMixin, APITestCase):
    """ test cases for datasources """

    def get_data_source(self):
        """ fetch data source """
        data_source = models.DataSource()
        data_source.type = self.data_source_type
        data_source.name = 'test_name'
        data_source.description = 'test_description'
        data_source.created_by = self.user
        data_source.last_modified_by = self.user
        data_source.save()
        return data_source

    @staticmethod
    def get_data_source_type():
        """ fetch data source type """
        # pylint: disable=no-member
        return models.DataSourceType.objects.filter(name='Amazon S3 Bucket').get()

    def setUp(self):
        """ setup sample data """
        super(DataSourceTestCase, self).setUp()
        self.data_source_type = self.get_data_source_type()
        self.data_source = self.get_data_source()

    # pylint: disable=invalid-name,redefined-builtin
    def get_expected_response_create(self, id):
        """ expected response for the create operation """
        return json.dumps({
            'id': id,
            'type': str(self.data_source_type.id),
            'name': 'Google Cloud Bucket',
            'description': 'Google Cloud Storage Service',
            'metadata': '{}',
            'created_by': self.user.id,
            'created_on': '2018-01-01T00:00:00Z',
            'last_modified_by': self.user.id,
            'last_modified_on': '2018-01-01T00:00:00Z',
        }, separators=(',', ':'))

    def test_create_datasource(self):
        """ test for create operation """

        # prepare data
        data = {
            'type': self.data_source_type.id,
            'name': 'Google Cloud Bucket',
            'description': 'Google Cloud Storage Service',
            'metadata': '{}',
            'created_by': self.user.id,
            'last_modified_by': self.user.id,
        }

        # hit api
        response = self.client.post('/datasources/', data, format='json')

        # expected_response
        expected_response = self.get_expected_response_create(response.data['id'])

        # verify response
        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertJSONEqual(response.content.decode(), expected_response)

    def get_expected_response_read(self, id):
        """ expected response for list operation """
        return json.dumps({
            'created_on': '2018-01-01T00:00:00Z',
            'name': 'test_name',
            'last_modified_on': '2018-01-01T00:00:00Z',
            'metadata': '',
            'last_modified_by': self.user.id,
            'created_by': self.user.id,
            'description': 'test_description',
            'id': str(id),
            'type': str(self.data_source_type.id),
        }, separators=(',', ':'))

    def test_read_datasource(self):
        """ test the read operation """

        # prepare data
        # data_source = self.data_source

        # hit api
        response = self.client.get('/datasources/{}'.format(self.data_source.id))

        # expected response
        expected_response = self.get_expected_response_read(self.data_source.id)

        # verify response
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertJSONEqual(response.content.decode(), expected_response)

    def get_expected_response_list(self):
        """ get expected response list """
        return json.dumps({
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'id': str(self.data_source.id),
                'name': 'test_name',
                'description': 'test_description',
                'metadata': '',
                'created_on': '2018-01-01T00:00:00Z',
                'last_modified_on': '2018-01-01T00:00:00Z',
                'type': str(self.data_source_type.id),
                'created_by': self.user.id,
                'last_modified_by': self.user.id,
            }],
        }, separators=(',', ':'))

    def test_list_datasource(self):
        """ test for list operation """

        # hit api
        response = self.client.get('/datasources/')

        # expected response
        expected_response = self.get_expected_response_list()

        # verify response
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertJSONEqual(response.content.decode(), expected_response)

    def get_expected_response_update(self, id):
        """ expected response for update operation """
        return json.dumps({
            'id': id,
            'type': str(self.data_source_type.id),
            'name': 'Google Cloud Bucket',
            'description': 'Google Cloud Storage Service',
            'metadata': '{}',
            'created_by': self.user.id,
            'created_on': '2018-01-01T00:00:00Z',
            'last_modified_by': self.user.id,
            'last_modified_on': '2018-01-01T00:00:00Z',
        }, separators=(',', ':'))

    def test_update_datasource(self):
        """ test the read operation """

        # prepare data
        data = {
            'type': self.data_source_type.id,
            'name': 'Google Cloud Bucket',
            'description': 'Google Cloud Storage Service',
            'metadata': '{}',
            'created_by': self.user.id,
            'last_modified_by': self.user.id,
        }

        # hit api
        response = self.client.put('/datasources/{}'.format(self.data_source.id),
                                   data,
                                   format='json')

        # expected_response
        expected_response = self.get_expected_response_update(response.data['id'])

        # verify response
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertJSONEqual(response.content.decode(), expected_response)

    def test_delete_datasource(self):
        """ test the read operation """

        # hit api
        response = self.client.delete('/datasources/{}'.format(self.data_source.id), )

        # verify response
        self.assertEqual(response.status_code, HTTP_204_NO_CONTENT)
