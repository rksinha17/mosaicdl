# -*- coding: utf-8 -*-
""" serializers associated with the datasources """
from rest_framework import serializers

from . import models


# pylint: disable=too-few-public-methods
class DataSourcesSerializer(serializers.ModelSerializer):
    """ Serializer associated with the model DataSource """

    class Meta:
        """ metadata associated with the datasource """
        model = models.DataSource
        fields = '__all__'
