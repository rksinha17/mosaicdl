# -*- coding: utf-8 -*-
""" apis associated with the datasources """
from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView,
)

from .models import DataSource
from .serializers import DataSourcesSerializer


class DataSourcesListCreateAPIView(ListCreateAPIView):
    """ Create new user or get the list of users """
    # pylint: disable=no-member
    queryset = DataSource.objects.all()
    serializer_class = DataSourcesSerializer

    def perform_create(self, serializer):
        """ write object to db """
        serializer.save(created_by=self.request.user,
                        last_modified_by=self.request.user)

# pylint: disable=too-many-ancestors
class DataSourcesRetrieveUpdateDestroyAPIView(RetrieveUpdateDestroyAPIView):
    """ Perform Read, Update, Delete on top of users """
    # pylint: disable=no-member
    queryset = DataSource.objects.all()
    serializer_class = DataSourcesSerializer

    def perform_update(self, serializer):
        """ update object to db """
        serializer.save(last_modified_by=self.request.user)
