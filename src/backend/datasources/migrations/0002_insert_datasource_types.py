# -*- coding: utf-8 -*-
# pylint: disable=invalid-name
""" migration script for inserting master records to datasource type """
from django.db import migrations

from datasources.models import DataSourceType


# pylint: disable=unused-argument
def insert_master_records(apps, schema_editor):
    """ create master records """
    amazon = DataSourceType()
    amazon.name = 'Amazon S3 Bucket'
    amazon.metadata = ['access_key', 'secret_key', 'bucket_name']
    amazon.save()


class Migration(migrations.Migration):
    """ migration script """
    dependencies = [
        ('datasources', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(insert_master_records),
    ]
