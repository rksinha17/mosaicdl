# -*- coding: utf-8 -*-
# pylint: disable=invalid-name
""" migration script for creating tables in the database """
import uuid

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models

import utils.fields


class Migration(migrations.Migration):
    """ migration script """

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='DataSource',
            fields=[
                ('id', models.UUIDField(
                    default=uuid.uuid4, editable=False, primary_key=True,
                    serialize=False)),
                ('name', models.CharField(max_length=100)),
                ('description', models.TextField()),
                ('metadata', utils.fields.JSONField()),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('last_modified_on', models.DateTimeField(auto_now=True)),
                ('created_by', models.ForeignKey(
                    editable=False, on_delete=django.db.models.deletion.CASCADE,
                    related_name='ds_created', to=settings.AUTH_USER_MODEL)),
                ('last_modified_by', models.ForeignKey(
                    editable=False, on_delete=django.db.models.deletion.CASCADE,
                    related_name='ds_modified', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='DataSourceType',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False,
                                        primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=100)),
                ('metadata', utils.fields.JSONField()),
            ],
        ),
        migrations.AddField(
            model_name='datasource',
            name='type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE,
                                    to='datasources.DataSourceType'),
        ),
    ]
