from django.urls import path, re_path

from . import views


urlpatterns = [
    path('', views.DataSourcesListCreateAPIView.as_view()),
    re_path('^(?P<pk>[a-z\-0-9]+)$', views.DataSourcesRetrieveUpdateDestroyAPIView.as_view())
]
