# -*- coding: utf-8 -*-
""" models associated with the datasources """
import uuid

from django.contrib.auth.models import User
from django.db import models

from utils.fields import JSONField


class DataSourceType(models.Model):
    """
    Table representing different data source types like Amazon S3 bucket,
    Google cloud bucket etc
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=100)
    metadata = JSONField()

    def __str__(self):
        """ string representation of the object """
        return self.name


class DataSource(models.Model):
    """ Table representing different sources of data """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    type = models.ForeignKey(DataSourceType, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    description = models.TextField()
    metadata = JSONField()
    created_on = models.DateTimeField(auto_now_add=True, editable=False)
    last_modified_on = models.DateTimeField(auto_now=True, editable=False)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE,
                                   related_name='ds_created', editable=False)
    last_modified_by = models.ForeignKey(User, on_delete=models.CASCADE,
                                         related_name='ds_modified', editable=False)

    def __str__(self):
        """ string representation of the object """
        return self.name
