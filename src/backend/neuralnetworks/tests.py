# -*- coding: utf-8 -*-
""" tests associated with the neural networks """
import json

from freezegun import freeze_time
from rest_framework.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_204_NO_CONTENT,
)
from rest_framework.test import APITestCase

from utils.mixins import TestMixin
from .models import NeuralNetwork


@freeze_time('2018-01-01T00:00:00Z')
class NeuralNetworkTestCase(TestMixin, APITestCase):
    """ test cases for neural network """

    def get_test_neural_network(self):
        """ fetch neural network """
        neural_network_obj = NeuralNetwork()
        neural_network_obj.name = 'Pradnya'
        neural_network_obj.description = 'Software Engineer'
        neural_network_obj.structure = '{}'
        neural_network_obj.create_on = '2018-01-01T00:00:00Z'
        neural_network_obj.created_by = self.user
        neural_network_obj.last_modified_on = '2018-01-01T00:00:00Z'
        neural_network_obj.last_modified_by = self.user
        neural_network_obj.save()
        return neural_network_obj

    def setUp(self):
        """ set up sample data for neural network"""
        super().setUp()
        self.neural_network = self.get_test_neural_network()

    # pylint: disable=invalid-name,redefined-builtin
    def get_expected_response_create(self, id):
        """ expected response for create operation """
        return json.dumps({
            'id': str(id),
            'name': 'test_name',
            'description': 'test_description',
            'structure': '{}',
            'create_on': '2018-01-01T00:00:00Z',
            'created_by': self.user.id,
            'last_modified_on': '2018-01-01T00:00:00Z',
            'last_modified_by': self.user.id
        }, separators=(',', ':'))

    def test_create(self):
        """ test to check create operation """

        # prepare data
        data = {'name': 'test_name', 'description': 'test_description', 'structure': '{}'}

        # hit api
        response = self.client.post('/neuralnetworks/', data, format='json')

        # expected response
        expected_response = self.get_expected_response_create(response.data['id'])

        # verify response
        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertJSONEqual(response.content.decode(), expected_response)

    # pylint: disable=invalid-name
    @staticmethod
    def get_expected_response_read(id):
        """ expected response for read operation """
        return json.dumps({
            'id': str(id),
            'name': 'Pradnya',
            'description': 'Software Engineer',
            'structure': '{}',
            'create_on': '2018-01-01T00:00:00Z',
            'created_by': 1,
            'last_modified_on': '2018-01-01T00:00:00Z',
            'last_modified_by': 1
        })

    def test_read(self):
        """ test read operation"""

        # hit api
        response = self.client.get('/neuralnetworks/{}'.format(self.neural_network.id))

        # expected response
        expected_response = self.get_expected_response_read(self.neural_network.id)

        # verify response
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertJSONEqual(response.content.decode(), expected_response)

    def get_expected_response_list(self):
        """ expected response for list operation """
        # pylint: disable=duplicate-code
        return json.dumps({
            'count': 1,
            'next': None,
            'previous': None,
            'results': [{
                'create_on': '2018-01-01T00:00:00Z',
                'created_by': self.user.id,
                'description': 'Software Engineer',
                'id': str(self.neural_network.id),
                'last_modified_by': self.user.id,
                'last_modified_on': '2018-01-01T00:00:00Z',
                'name': 'Pradnya',
                'structure': '{}'}]
        })

    def test_list(self):
        """ test list operation """

        # hit api
        response = self.client.get('/neuralnetworks/')

        # expected response
        expected_response = self.get_expected_response_list()

        # verify response
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertJSONEqual(response.content.decode(), expected_response)

    # pylint: disable=invalid-name
    def get_expected_response_update(self, id):
        """ expected response for update operation """
        return json.dumps({
            'id': id,
            'create_on': '2018-01-01T00:00:00Z',
            'created_by': self.user.id,
            'description': 'senior Software Engineer',
            'last_modified_by': self.user.id,
            'last_modified_on': '2018-01-01T00:00:00Z',
            'name': 'pradnya k',
            'structure': '{}'
        }, separators=(',', ':'))

    def test_update(self):
        """ test update opeartion"""

        # prepare data
        data = {
            'name': 'pradnya k',
            'description': 'senior Software Engineer',
            'structure': '{}',
            'created_by': self.user.id,
            'modified_by': self.user.id,
        }

        # hit api
        response = self.client.put('/neuralnetworks/{}'.format(
            self.neural_network.id), data, format='json')

        # expected response
        expected_response = self.get_expected_response_update(response.data['id'])

        # verify response
        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertJSONEqual(response.content.decode(), expected_response)

    def test_delete(self):
        """ test delete operation """

        # hit api
        response = self.client.delete('/neuralnetworks/{}/'.format(self.neural_network.id))

        # verify response
        self.assertTrue(response.status_code, HTTP_204_NO_CONTENT)
