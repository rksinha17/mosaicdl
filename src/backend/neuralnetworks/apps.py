# -*- coding: utf-8 -*-
""" django application registry """
from django.apps import AppConfig


class NeuralNetworkConfig(AppConfig):
    """ django app configuration """
    name = 'neuralnetworks'
