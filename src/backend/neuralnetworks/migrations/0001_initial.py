# -*- coding: utf-8 -*-
# pylint: disable=invalid-name
""" create tables associated with neural networks """
import uuid

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion

import utils.fields


class Migration(migrations.Migration):
    """ migration script """
    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='NeuralNetwork',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False,
                                        primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=70)),
                ('description', models.CharField(max_length=300)),
                ('structure', utils.fields.JSONField()),
                ('create_on', models.DateTimeField(auto_now_add=True)),
                ('last_modified_on', models.DateTimeField(auto_now=True)),
                ('created_by', models.ForeignKey(
                    editable=False, on_delete=django.db.models.deletion.CASCADE,
                    related_name='nn_created', to=settings.AUTH_USER_MODEL)),
                ('last_modified_by', models.ForeignKey(
                    editable=False, on_delete=django.db.models.deletion.CASCADE,
                    related_name='nn_modified', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
