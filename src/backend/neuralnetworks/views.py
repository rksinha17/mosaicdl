# -*- coding: utf-8 -*-
""" apis associated with neural networks"""
from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView,
)

from .models import NeuralNetwork
from .serializers import NeuralNetworkSerializer


class NeuralNetworkListCreateAPIView(ListCreateAPIView):
    """ Create new user or get the list of users """
    # pylint: disable=no-member
    queryset = NeuralNetwork.objects.all()
    serializer_class = NeuralNetworkSerializer

    def perform_create(self, serializer):
        """ write object to db """
        serializer.save(created_by=self.request.user,
                        last_modified_by=self.request.user)


# pylint: disable=too-many-ancestors
class NeuralNetworkRetrieveUpdateDestroyAPIView(RetrieveUpdateDestroyAPIView):
    """ Perform Read, Update, Delete on top of users """
    # pylint: disable=no-member
    queryset = NeuralNetwork.objects.all()
    serializer_class = NeuralNetworkSerializer

    def perform_update(self, serializer):
        """ update object to db """
        serializer.save(last_modified_by=self.request.user)
