# -*- coding: utf-8 -*-
from django.urls import path, re_path

from . import views


urlpatterns = [
    path('', views.NeuralNetworkListCreateAPIView.as_view()),
    re_path('^(?P<pk>[a-z\-0-9]+)$', views.NeuralNetworkRetrieveUpdateDestroyAPIView.as_view())
]
