# -*- coding: utf-8 -*-
""" serializers associated with the user model """
from rest_framework import serializers

from .models import NeuralNetwork


class NeuralNetworkSerializer(serializers.ModelSerializer):
    """ serializer for the neural network """

    # pylint: disable=too-few-public-methods,
    class Meta:
        """ metadata associated with neural network serializer """
        model = NeuralNetwork
        fields = ('id', 'name', 'description', 'structure', 'create_on', 'created_by',
                  'last_modified_on', 'last_modified_by')
