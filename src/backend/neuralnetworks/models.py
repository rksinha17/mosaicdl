# -*- coding: utf-8 -*-
""" models for neural networks """
import uuid

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from git import Repo
from utils.fields import JSONField
from celery import shared_task
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.conf import settings
from utils.git import create_repo, checkout, create_file, push_file, git_list, pull_file




import os


# getting required git parameters
private_key = getattr(settings, "PRIVATE_DIR", None)
public_key = getattr(settings, "PUBLIC_DIR", None)
remote_dir_url = getattr(settings, "GIT_DIR", None)

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SERVER_DIR = (
    '/home/ron/test-repo'
)


class NeuralNetwork(models.Model):
    """ Table representing neural network """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=70)
    description = models.CharField(max_length=300)
    structure = JSONField()
    create_on = models.DateTimeField(auto_now_add=True, editable=False)
    created_by = models.ForeignKey(User, related_name='nn_created',
                                   on_delete=models.CASCADE, editable=False)
    last_modified_on = models.DateTimeField(auto_now=True, editable=False)
    last_modified_by = models.ForeignKey(User, related_name='nn_modified',
                                         on_delete=models.CASCADE,
                                         editable=False)

    # starting a backend task on getting signal

    @shared_task
    @receiver(post_save, sender=User)
    def save_profile(sender, **kwargs):
        if kwargs ['created']:
            x = id
            local_cloned_dir = "/tmp/{}".format(x)

            # create repository if not present
            create_repo( name =x,public_key=public_key, private_key = private_key, remote_dir_url =remote_dir_url )



            # changing working directory
            try:
                os.chdir(local_cloned_dir)
            except ValueError:
                print("Error moving to cloned repo")
                exit(100)

            # git checkout
            checkout(local_cloned_dir=local_cloned_dir)


            # creating a file to be pushed
            create_file(local_cloned_dir=local_cloned_dir)


            # Pushing the in the remote repository
            push_file(local_cloned_dir=local_cloned_dir)


            # get the git list in a file
            git_list(local_cloned_dir=local_cloned_dir)

            # pull the lastest from git repository
            pull_file(local_cloned_dir=local_cloned_dir)




