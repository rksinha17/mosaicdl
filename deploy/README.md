# Prerequisites

## Nginx

```
yum install nginx -y
```

## Mariadb

Setup `/etc/yum.repos.d/mariadb.repo` with the following,

```
# MariaDB 10.2 CentOS repository list - created 2018-01-30 11:35 UTC
# http://downloads.mariadb.org/mariadb/repositories/
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.2/centos7-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
```

Now install mariadb using the following,
```
sudo yum install MariaDB-server MariaDB-client MariaDB-shared
```

Now create a databasse for mosaicdl
```
mysql -uroot -p
create database mosaicdl;
exit;
```

## Node

```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash
source ~/.bashrc
nvm install node
```

## Python 3.6 (with openssl support)

```
yum groupinstall "Development tools" -y
yum install openssl openssl-devel -y
wget https://www.python.org/ftp/python/3.6.4/Python-3.6.4.tgz
tar xzvf Python-3.6.4
cd Python-3.6.4
./configure --prefix=/usr/local/
make
make install
```

## Pip

```
wget https://bootstrap.pypa.io/get-pip.py
python3.6 get-pip.py
```

# Applying configuration files

```
pip install cookiecutter
cookiecutter .
```

# Architecture

![architecture-diagram](https://bitbucket.org/maxiqdev/mosaicdl/raw/master/deploy/architecture.png)
